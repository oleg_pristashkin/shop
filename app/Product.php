<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use PhpParser\JsonDecoder;
use Illuminate\Support\Facades\App;

class Product extends Model
{
    private const getUrl = 'https://b2b-sandi.com.ua/api/products';

    protected $fillable = [
        'sku',
        'name_ru',
        'description_ru',
        'name_uk',
        'description_uk',
        'price'
    ];

    /**
     * imports data from b2b to the database
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function importFromB2B(): bool
    {
        $client = new Client();
        $response = $client->request('GET', self::getUrl);
        $productsJsonString = $response->getBody();

        if (!$productsJsonString) {
            return false;
        }

        $decoder = new JsonDecoder();
        $products = $decoder->decode($productsJsonString);

        foreach ($products as $productData) {
            $product = self::create($productData);

            array_walk($productData['characteristics'], function (&$item){
                $item = new ProductCharacteristic($item);
            });

            $product->characterisitcs()->saveMany($productData['characteristics']);

        }

        return true;
    }

    public function getNameAttribute()
    {
        $name = 'name_' . App::getLocale();
        return $this->$name;
    }

    public function getDescriptionAttribute()
    {
        return $this->{'description_' . App::getLocale()};
    }

    public function characterisitcs()
    {
        return $this->hasMany(ProductCharacteristic::class);
    }

}

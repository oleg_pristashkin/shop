<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

class ProductCharacteristic extends Model
{

    protected $fillable = [
        'name_ru',
        'value_ru',
        'name_uk',
        'value_uk'
    ];

    public function getNameAttribute(){
        $name = 'name_' . App::getLocale();
        return $this->$name;
    }

    public function getValueAttribute(){
        return $this->{'value_' . App::getLocale()};
    }
}

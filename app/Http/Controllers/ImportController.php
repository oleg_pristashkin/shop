<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ImportController extends Controller
{

    public function importProducts(Request $request)
    {
        Product::importFromB2B();
        return redirect()->back();
    }
}

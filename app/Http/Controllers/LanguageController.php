<?php

namespace App\Http\Controllers;

use App\Helpers\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function switch(Request $request)
    {
        Session::put('locale', $request['lang']);
        return redirect()->back();
    }
}

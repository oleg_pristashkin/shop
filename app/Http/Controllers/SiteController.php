<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $products = Product::paginate(15);
        return view('site.index', ['products' => $products]);
    }

    public function product($id)
    {
        $product = Product::find($id);
        return view('site.product', ['product' => $product]);
    }
}

@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
    <div class="row">

    </div>
    <div class="row">
        <div class="col-md-2">
            {{ __('messages.name') }}
        </div>
        <div class="col-md-10">
            {{ $product->name }}
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-2">
            {{ __('messages.description') }}
        </div>
        <div class="col-md-10">
            {{ $product->description }}
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-2">
            {{ __('messages.price') }}
        </div>
        <div class="col-md-10">
            {{ $product->price }}
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-2">
            {{ __('messages.characteristics') }}
        </div>
        <div class="col-md-10">

        </div>
    </div>
    <br/>
    @foreach($product->characterisitcs as $characteristic)
        <div class="row">
            <div class="col-md-2">
                {{ $characteristic->name }}
            </div>
            <div class="col-md-10">
                {{ $characteristic->description }}
            </div>
        </div>
    @endforeach
@endsection

@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
    <a href="{{ route('product.import') }}" class="btn btn-primary"><span class="fa fa-download"> {{ __('messages.import products') }}</span></a>
    <br/><br/>
    @foreach($products as $product)

        <a href="{{route('site.product', $product->id )}}">{{$product->name}}</a>

        <br/>
    @endforeach
    {{$products->links()}}
@endsection

<?php

return [
    'en' => 'Англ',
    'ru' => 'Рус',
    'uk' => 'Укр',
    'import products' => 'Импортировать продукты',
    'name' => 'Имя',
    'description' => 'Описание',
    'price' => 'Цена',
    'characteristics' => 'Характеристики'
];

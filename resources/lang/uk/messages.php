<?php

return [
    'en' => 'Англ',
    'ru' => 'Рос',
    'uk' => 'Укр',
    'import products' => 'Iмпортувати продукти',
    'name' => 'Iм\'я',
    'description' => 'Опис',
    'price' => 'Цiна',
    'characteristics' => 'Характеристики'
];
